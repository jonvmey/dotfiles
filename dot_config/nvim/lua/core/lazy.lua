local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"

if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable",
    lazypath,
  })
end

vim.opt.rtp:prepend(lazypath)

local plugins = {
  "lewis6991/gitsigns.nvim",
  {
    "lukas-reineke/indent-blankline.nvim",
    main = "ibl",
    opts = {},
  },
  "mbbill/undotree",
  {
    "numToStr/Comment.nvim",
    opts = {},
    lazy = false,
  },
  {
    "nvim-lualine/lualine.nvim",
    dependencies = { "nvim-tree/nvim-web-devicons" },
  },
  {
    "nvim-telescope/telescope.nvim",
    tag = "0.1.5",
    dependencies = { { "nvim-lua/plenary.nvim" } },
  },
  {
    "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate",
  },
  "nvim-treesitter/nvim-treesitter-context",
  {
    "saecki/crates.nvim",
    tag = "stable",
    event = { "BufRead Cargo.toml" },
    config = function()
      require("crates").setup()
    end,
  },
  {
    "tomasr/molokai",
    lazy = false,
    priority = 1000, -- make sure to load this before all other start plugins
    config = function()
      vim.cmd.colorscheme("molokai")
    end,
  },
  "tpope/vim-fugitive",
  "tpope/vim-repeat",
  "tpope/vim-surround",
  {
    "VonHeikemen/lsp-zero.nvim",
    dependencies = { {
      "L3MON4D3/LuaSnip",
      "hrsh7th/cmp-buffer",
      "hrsh7th/cmp-nvim-lsp",
      "hrsh7th/cmp-nvim-lua",
      "hrsh7th/cmp-path",
      "hrsh7th/nvim-cmp",
      "neovim/nvim-lspconfig",
      "rafamadriz/friendly-snippets",
      "saadparwaiz1/cmp_luasnip",
      "williamboman/mason-lspconfig.nvim",
      "williamboman/mason.nvim",
    } },
  },
  {
    "windwp/nvim-autopairs",
    event = "InsertEnter",
    opts = {},
  },
  {
    "NoahTheDuke/vim-just",
    ft = { "just" },
  },
}

local opts = {}

require("lazy").setup(plugins, opts)
