local keymap = function(mode, from, to, extra_opts)
  local opts = { noremap = true, silent = true }

  if extra_opts ~= nil then
    for k, v in pairs(extra_opts) do
      opts[k] = v
    end
  end

  vim.keymap.set(mode, from, to, opts)
end

local nnoremap = function(from, to, extra_opts)
  keymap("n", from, to, extra_opts)
end
local vnoremap = function(from, to, extra_opts)
  keymap("v", from, to, extra_opts)
end
local inoremap = function(from, to, extra_opts)
  keymap("i", from, to, extra_opts)
end
local cnoremap = function(from, to, extra_opts)
  keymap("c", from, to, extra_opts)
end
local xnoremap = function(from, to, extra_opts)
  keymap("x", from, to, extra_opts)
end

-- open netrw
nnoremap("<leader>pv", vim.cmd.Ex)

-- easier commands
nnoremap(";", ":", { silent = false })

-- close and save files
nnoremap("K", "<cmd>q<CR>")
nnoremap("s", "<cmd>w<CR>")

-- open "Manual"
nnoremap("M", "K")

-- easy home row escape sequence
inoremap("jk", "<esc>")
inoremap("Jk", "<esc>")
inoremap("JK", "<esc>")

--
nnoremap("<leader>s", "vip:!sort<cr>")
vnoremap("<leader>s", ":!sort<cr>")

nnoremap("<leader>W", "<cmd>set wrap!<CR>")

nnoremap("<CR>", "o<esc>")

inoremap("<C-y>", "<esc>mzgUiw`za")

vnoremap("J", "<cmd>move '>+1<CR>gv=gv")
vnoremap("K", "<cmd>move '<-2<CR>gv=gv")

nnoremap("S", "i<CR><esc>^mzgk<cmd>silent! s/\v +$//<CR><cmd>nohlsearch<CR>`z")
nnoremap("J", "mzJ`z")

nnoremap("<C-u>", "<C-u>zz")
nnoremap("<C-d>", "<C-d>zz")
nnoremap("n", "nzzzv")
nnoremap("N", "Nzzzv")

nnoremap("*", "<cmd>let stay_star_view = winsaveview()<CR>*<cmd>call winrestview(stay_star_view)<CR>")

xnoremap("<leader>p", "\"_dP")

nnoremap("<leader>y", "\"+y")
vnoremap("<leader>y", "\"+y")
nnoremap("<leader>Y", "\"+Y")

nnoremap("<leader>d", "\"_d")
vnoremap("<leader>d", "\"_d")

nnoremap("Q", "<nop>")

nnoremap("/", "/\\v", { silent = false })
vnoremap("/", "/\\v", { silent = false })

nnoremap("<leader><space>", "<cmd>nohlsearch<CR>call clearmatches()<CR>")

nnoremap("<leader>S", ":%s/\\<<C-r><C-w>\\>/<C-r><C-w>/gI<Left><Left><Left>")
nnoremap("<C-s>", ":%s/", { silent = false })
vnoremap("<C-s>", ":s/", { silent = false })

nnoremap("<leader>wf", "mz<cmd>%s/\\s\\+$//<CR><cmd>let @/=''<CR>`z")

nnoremap("vv", "^vg_")

nnoremap("H", "^")
vnoremap("H", "^")
nnoremap("L", "$")
vnoremap("L", "g_")

nnoremap("'", "`")
nnoremap("`", "<C-^>")

inoremap("<C-a>", "<esc>I")
inoremap("<C-e>", "<esc>A")
cnoremap("<C-a>", "<Home>")
cnoremap("<C-e>", "<End>")

nnoremap("j", "gj")
nnoremap("k", "gk")
nnoremap("gj", "j")
nnoremap("gk", "k")

nnoremap("<C-h>", "<C-w>h")
nnoremap("<C-j>", "<C-w>j")
nnoremap("<C-k>", "<C-w>k")
nnoremap("<C-l>", "<C-w>l")

nnoremap("<leader>v", "<C-w>v")
nnoremap("<leader>h", "<C-w>s")
