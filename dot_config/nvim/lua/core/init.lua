require("core.globals")
require("core.settings")
require("core.keymap")
require("core.lazy")

require("Comment").setup()
require("lualine").setup()
require("gitsigns").setup()
