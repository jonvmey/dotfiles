local settings = {
  splitbelow = true,
  splitright = true,

  cursorline = true,

  number = true,
  relativenumber = true,

  autoread = true,
  autowrite = true,

  title = true,

  linebreak = true,
  showbreak = "..",
  breakindent = true,

  nrformats = "bin,hex,octal",

  tabstop = 2,
  softtabstop = 2,
  shiftwidth = 2,
  expandtab = true,

  swapfile = false,
  backup = false,
  undofile = true,

  termguicolors = true,

  scrolloff = 8,
  signcolumn = "yes",

  updatetime = 50,

  textwidth = 80,
  colorcolumn = "+1",

  ignorecase = true,
  smartcase = true,
}

for k, v in pairs(settings) do
  vim.opt[k] = v
end

vim.opt.isfname:append("@-@")
