# Settings
set -s escape-time 0

set -g history-limit 50000

set -g display-time 4000

set -g focus-events on

set -g mouse on

# Enable CTRL-arrow keys to function properly
set-window-option -g xterm-keys on

set -g mode-keys vi
set -g status-keys emacs

set -g default-terminal "tmux-256color"

set -g main-pane-width 260

# Key bindings
bind C-p previous-window
bind C-n next-window

bind S choose-session

bind v split-window -h -c '#{pane_current_path}'
bind s split-window -v -c '#{pane_current_path}'

bind C-h select-pane -L
bind C-j select-pane -D
bind C-k select-pane -U
bind C-l select-pane -R

bind 1 select-pane -t 1
bind 2 select-pane -t 2
bind 3 select-pane -t 3
bind 4 select-pane -t 4
bind 5 select-pane -t 5
bind 6 select-pane -t 6
bind 7 select-pane -t 7
bind 8 select-pane -t 8
bind 9 select-pane -t 9

bind M select-layout main-vertical
bind E select-layout even-horizontal

bind -r h resize-pane -L 5
bind -r j resize-pane -D 5
bind -r k resize-pane -U 5
bind -r l resize-pane -R 5

bind -r H select-window -t :-
bind -r L select-window -t :+

bind R source-file ~/.tmux.conf \; display "Reloaded ~/.tmux.conf"

bind-key -r f run-shell "tmux neww ~/.local/bin/tmux-sessionizer"

# Plugins
set -g @plugin 'tmux-plugins/tpm'

set -g @plugin 'tmux-plugins/tmux-resurrect'

set -g @plugin 'tmux-plugins/tmux-continuum'
set -g @continuum-boot 'on'
set -g @continuum-restore 'on'

run '~/.tmux/plugins/tpm/tpm'
